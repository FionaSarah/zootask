from unittest.mock import patch
from django.test import TestCase, Client
from django.core.exceptions import ValidationError
from django.utils.dateparse import parse_datetime
from django.utils.timezone import datetime, utc
from .models import Species, Animal


class SpeciesModelTestCase(TestCase):

    def test_species_errors_empty_name(self):
        instance = Species()
        self.assertRaises(ValidationError, instance.clean)

    def test_species_errors_on_duplicate(self):
        Species(name="Stegosaurus").save()
        instance = Species(name="Stegosaurus")
        self.assertRaises(ValidationError, instance.clean)

    def test_species_creates_when_valid(self):
        Species(name="Diplodocus").save()
        instance = Species(name="Pteradon")
        instance.clean()
        instance.save()


class AnimalModelTestCase(TestCase):

    def setUp(self):
        self.species = Species(name="Dinosaur")
        self.species.save()

    def test_animal_errors_empty_name(self):
        instance = Animal()
        self.assertRaises(ValidationError, instance.clean)

    def test_animal_errors_on_duplicate(self):
        Animal(name="Fluffy", species=self.species).save()
        instance = Animal(name="Fluffy", species=self.species)
        self.assertRaises(ValidationError, instance.clean)

    def test_animal_errors_on_no_species(self):
        instance = Animal(name="Fluffy")
        self.assertRaises(ValidationError, instance.clean)

    def test_animal_creates_when_valid(self):
        Animal(name="Fluffy", species=self.species).save()
        instance = Animal(name="Betty", species=self.species)
        instance.clean()
        instance.save()


class AnimalPopulationViewTestCase(TestCase):

    def setUp(self):
        self.species = Species(name="Dinosaur")
        self.species.save()

    def test_gets_0_animals(self):
        c = Client()
        response = c.get('/animals/population/')
        self.assertEqual(response.content, b"0")

    def test_gets_some_animals(self):
        s = Species(name="Dinosaur")
        s.save()
        Animal(name="Betty", species=s).save()
        Animal(name="Brian", species=s).save()
        Animal(name="Notaraptor", species=s).save()
        c = Client()
        response = c.get('/animals/population/')
        self.assertEqual(response.content, b"3")


class AnimalViewTestCase(TestCase):

    def setUp(self):
        s = Species(name="Dinosaur")
        s.save()
        Animal(name="Brian", species=s).save()

    def test_retrieves_animal(self):
        c = Client()
        json = c.get('/animals/animal/Brian/').json()
        self.assertEqual(json['name'], "Brian")
        self.assertEqual(json['species'], "Dinosaur")
        self.assertIsNotNone(parse_datetime(json['last_feed_time']))

    def test_404_on_animal_not_exists(self):
        c = Client()
        response = c.get('/animals/animal/Madeupname/')
        self.assertEqual(response.status_code, 404)

    def test_creates_an_animal(self):
        c = Client()
        response = c.post('/animals/animal/', {'name': "Something", 'species': 'Dinosaur'})
        self.assertEqual(response.status_code, 201)

    def test_fails_to_create_without_params(self):
        c = Client()
        response = c.post('/animals/animal/')
        self.assertEqual(response.status_code, 422)

    def test_fails_to_create_if_species_not_exist(self):
        c = Client()
        response = c.post('/animals/animal/', {'name': "Something", 'species': "Blah"})
        self.assertEqual(response.status_code, 422)

    def test_fails_to_create_if_duplicate(self):
        c = Client()
        response = c.post('/animals/animal/', {'name': "Brian", 'species': 'Dinosaur'})
        self.assertEqual(response.status_code, 422)


class FeedAnimalViewTestCase(TestCase):

    def setUp(self):
        s = Species(name="Dinosaur")
        s.save()
        Animal(name="Brian", species=s, last_feed_time=datetime(year=2018, month=2, day=1, tzinfo=utc)).save()

    def test_fails_if_animal_not_exist(self):
        c = Client()
        response = c.post('/animals/feed/', {'name': "Chippy"})
        self.assertEqual(response.status_code, 404)

    @patch('animals.views.datetime')
    def test_animal_is_fed(self, mocked_datetime):
        a_cool_day = datetime(year=2018, month=3, day=16, tzinfo=utc)
        mocked_datetime.now.return_value = a_cool_day
        mocked_datetime.utcnow.return_value = a_cool_day
        c = Client()
        response = c.post('/animals/feed/', {'name': "Brian"})
        self.assertEqual(response.status_code, 201)
        animal = Animal.objects.get(name="Brian")
        self.assertEqual(animal.last_feed_time, a_cool_day)


class HungryAnimalsViewTestCase(TestCase):

    def setUp(self):
        s = Species(name="Dinosaur")
        s.save()
        Animal(name="Brian", species=s, last_feed_time=datetime(year=2018, month=3, day=15, tzinfo=utc)).save()
        Animal(name="Betty", species=s, last_feed_time=datetime(year=2018, month=3, day=16, tzinfo=utc)).save()
        Animal(name="Suzie", species=s, last_feed_time=datetime(year=2018, month=2, day=12, tzinfo=utc)).save()
        Animal(name="Chippy", species=s, last_feed_time=datetime(year=2018, month=3, day=14, tzinfo=utc)).save()

    @patch('animals.views.datetime')
    def test_number_hungry_animals(self, mocked_datetime):
        a_cool_day = datetime(year=2018, month=3, day=16, tzinfo=utc)
        mocked_datetime.now.return_value = a_cool_day
        mocked_datetime.utcnow.return_value = a_cool_day
        c = Client()
        response = c.get('/animals/hungry/')
        self.assertEqual(response.content, b'2')

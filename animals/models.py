from django.utils import timezone
from django.db import models
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.utils.translation import gettext_lazy as _


class AnimalBaseClass(models.Model):
    """
    Base class for animal models.

    In kayfabe, the designer of these classes didn't add any unique constraints.
    The test told me it should be done in the view, but IMO that is the purview of
    the model not the view. So that's why I'm doing the validation here.
    """
    empty_name_validation_message = ""
    species_exists_validation_message = ""

    class Meta:
        abstract = True

    def clean(self):
        # Check name isn't empty
        self.name = self.name.strip()
        if self.name == "":
            raise ValidationError(self.empty_name_validation_message)
        # Check for duplicates
        try:
            self.__class__.objects.get(name=self.name)
            raise ValidationError(self.species_exists_validation_message)
        except ObjectDoesNotExist:
            pass


class Species(AnimalBaseClass):
    """
    Each class of animal
    """
    name = models.CharField(max_length=30, primary_key=True)

    empty_name_validation_message = _("Species must have a name")
    species_exists_validation_message = _("A species with this name already exists")

    def __str__(self):
        return self.name


class Animal(AnimalBaseClass):
    """
    Fluffy little hungry faces
    """
    name = models.CharField(max_length=30, primary_key=True)
    species = models.ForeignKey(Species, on_delete=models.CASCADE)
    last_feed_time = models.DateTimeField(default=timezone.now)

    empty_name_validation_message = _("Animal must have a name")
    species_exists_validation_message = _("An animal with this name already exists")

    def clean(self):
        super().clean()
        if not self.species_id:
            raise ValidationError(_("Animals must have a species"))

    def __str__(self):
        # Translators: A named representation of an animal. Parameters are name and species.
        return _("%s the %s") % (self.name, self.species.name)

from django.urls import path
from .views import AnimalPopulationView, AnimalView, HungryAnimalsView, FeedAnimalView


urlpatterns = [
    path('population/', AnimalPopulationView.as_view(), name='population'),
    path('animal/', AnimalView.as_view(), name='animal'),
    path('animal/<str:name>/', AnimalView.as_view(), name='animal'),
    path('feed/', FeedAnimalView.as_view(), name='feed'),
    path('hungry/', HungryAnimalsView.as_view(), name='hungry'),
]
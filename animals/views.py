import json
from django.conf import settings
from django.http import HttpResponse
from django.views.generic.base import View
from django.shortcuts import get_object_or_404
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ValidationError
from django.utils.timezone import datetime, timedelta
from .models import Animal, Species


class AnimalPopulationView(View):
    """View for reporting on total number of animals"""
    def get(self, request, *args, **kwargs):
        return HttpResponse(Animal.objects.count())


class AnimalView(View):
    """
    View for individual animals.
    """
    def get(self, request, *args, **kwargs):
        """Get information on an animal."""
        animal = get_object_or_404(Animal, name=kwargs["name"])
        response = {'name': animal.name, 'species': str(animal.species), 'last_feed_time': str(animal.last_feed_time)}
        return HttpResponse(json.dumps(response), content_type="application/json")

    def post(self, request, *args, **kwargs):
        """Create new animal."""
        # Check passed params
        if "name" not in request.POST or "species" not in request.POST:
            return HttpResponse(_("Please supply name and species to add animal."), status=422)
        # Check specified species exists
        try:
            species = Species.objects.get(name=request.POST['species'])
        except Species.DoesNotExist:
            return HttpResponse(_("Species supplied does not exist."), status=422)
        # Try creating animal
        animal = Animal(name=request.POST['name'], species=species)
        try:
            animal.clean()
        except ValidationError as e:
            return HttpResponse(e.message, status=422)
        animal.save()
        # Translators: Animal adding conformation message. Parameter is the animal.
        return HttpResponse(_("Animal added: %s") % animal, status=201)


class HungryAnimalsView(View):
    """
    View for showing animals that haven't eaten in the last 2 days.
    """
    def get(self, request, *args, **kwargs):
        """How many to feed pls"""
        two_days_ago = datetime.utcnow() - timedelta(days=settings.ANIMAL_HUNGRY_THRESHOLD)
        return HttpResponse(Animal.objects.filter(last_feed_time__lte=two_days_ago).count())


class FeedAnimalView(View):
    """
    View for feeding an animal.
    """
    def post(self, request, *args, **kwargs):
        """Feed animal."""
        # Check passed params
        if "name" not in request.POST:
            return HttpResponse(_("Please supply name to feed animal."), status=422)
        animal = get_object_or_404(Animal, name=request.POST['name'])
        animal.last_feed_time = datetime.utcnow()
        animal.save()
        # Translators: Animal feeding conformation message. Parameter is the animal.
        return HttpResponse(_("Animal fed: %s") % animal, status=201)

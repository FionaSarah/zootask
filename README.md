Zoo Task
========

I got given a small technical test to make a feed-yo-zoo-animals manager in django to a specification.

I'm not providing the specification so this isn't easily Googleable for anyone else doing this.

Installation
------------

* Check it out
* Use a virtualenv or something similar
* `pip install -r requirements.txt`
* Create a Postgres database and edit the settings in `zootask/settings.py`
* `python manage.py migrate`
* Feed some animals.

Running Tests
-------------

`manage.py test`

Comments
--------

This ended up being a "Make a micro API" which is cool and all, but it's not RESTful or very complete and to be honest 
I would have just used Django Rest Framework myself, but the specifications were very clear that I should only use plain
Python+Django.